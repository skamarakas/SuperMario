﻿using MarioObjects.Objects.BaseObjects;

namespace MarioObjects.Objects.IntersectionStrategy
{
    public class BasicIntersectionAlgorithm : IIntersertionAlgorithm
    {
        public void CustomIntersection(Collision c, GraphicObject g, MoveableAnimatedObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_BlockQuestion:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_Grass:
                    {
                        if (c.Dir == CollisionDirection.CD_Up)
                        {
                            obj.Fall = false;
                        }

                    }
                    break;
                case ObjectType.OT_Brick:
                    goto case ObjectType.OT_PipeUp;
                case ObjectType.OT_Pirana:
                    goto case ObjectType.OT_PipeUp;
                case ObjectType.OT_SolidBlock:
                    goto case ObjectType.OT_PipeUp;
                case ObjectType.OT_PipeUp:
                    {
                        if (c.Dir == CollisionDirection.CD_Left || c.Dir == CollisionDirection.CD_Right)
                        {
                            obj.Fall = false;
                            obj.DirX *= -1;
                            obj.OnWalk(null, null);
                        }

                    }
                    break;

            }
        }
    }
}
