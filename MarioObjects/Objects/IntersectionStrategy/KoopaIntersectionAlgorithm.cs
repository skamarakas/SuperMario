﻿using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects;

namespace MarioObjects.Objects.IntersectionStrategy
{
    public class KoopaIntersectionAlgorithm : IIntersertionAlgorithm
    {
        public void CustomIntersection(Collision c, GraphicObject g, MoveableAnimatedObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_Brick:
                    {
                        if (obj.State == KoopaState.KS_ShieldMoving)
                        {
                            if (c.Dir == CollisionDirection.CD_Left || c.Dir == CollisionDirection.CD_Right)
                                ((BlockBrick)g).BreakBrick();
                        }
                    }
                    break;
                case ObjectType.OT_Goomba:
                    {
                        if (obj.State == KoopaState.KS_ShieldMoving)
                        {
                            ((MonsterGoomba)g).GoombaFallDie();
                        }
                        if (obj.State == KoopaState.KS_Walking)
                        {
                            ((MonsterGoomba)g).DirX *= -1;
                            ((MonsterGoomba)g).newx += 5 * ((MonsterGoomba)g).DirX;

                            ((MonsterGoomba)g).OnWalk(null, null);
                            obj.DirX *= -1;
                            obj.OnWalk(null, null);
                        }
                        if (obj.State == KoopaState.KS_Shield)
                        {
                            ((MonsterGoomba)g).DirX *= -1;
                            ((MonsterGoomba)g).newx += 5 * ((MonsterGoomba)g).DirX;
                            ((MonsterGoomba)g).OnWalk(null, null);
                        }
                    }
                    break;
                case ObjectType.OT_Mario:
                    {

                        if (obj.State == KoopaState.KS_Shield && obj.ReturningTime >= 3)
                        {
                            if (c.Dir == CollisionDirection.CD_Left)
                                obj.DirX = -1;

                            if (c.Dir == CollisionDirection.CD_Right)
                                obj.DirX = 1;

                            obj.SetKoopaState(KoopaState.KS_ShieldMoving);
                        }

                        // Size-down mario when colliding with a koopa
                        if (State != KoopaState.KS_Shield) // but not in shield state
                        {
                            if (!(State == KoopaState.KS_ShieldMoving // Or that he's just set in motion
                                && (obj.DirX == -1 && c.Dir == CollisionDirection.CD_Left) || (obj.DirX == 1 && c.Dir == CollisionDirection.CD_Right)))
                            {
                                Mario m = (Mario)g;
                                if (c.Dir != CollisionDirection.CD_Down)
                                {
                                    if (!m.Blinking)
                                        if (m.Type == Mario.MarioType.MT_Big || m.Type == Mario.MarioType.MT_Fire)
                                        {
                                            m.Type = Mario.MarioType.MT_Small;
                                            m.StartBlinking();
                                            m.SetMarioProperties();
                                        }
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }
}
