﻿using MarioObjects.Objects.BaseObjects;
using System;

namespace MarioObjects.Objects.IntersectionStrategy
{
    interface IIntersertionAlgorithm
    {

        void CustomIntersection(Collision c, GraphicObject g, MoveableAnimatedObject obj);
    }
}
