﻿using System;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects.Strategy;
using MarioObjects.Objects.GameObjects.State;

namespace MarioObjects.Objects.GameObjects
{
    public class MonsterKoopa : MoveableAnimatedObject
    {
        private KoopaWalkingState walking;
        private KoopaShieldState shield;
        private KoopaReturningState returning;
        private KoopaShieldMovingState shieldMoving;

        private IKoopaState current;

        public enum KoopaState { KS_Walking, KS_Shield, KS_Returning, KS_ShieldMoving };
        public enum KoopaDir { KD_Right, KD_Left };
        public KoopaDir Dir;
        public int ReturningTime;
        public static LevelEditorObject GetLEObject()
        {
            return new LevelEditorObject(16, 27, 10, 2, ObjectType.OT_Koopa, null);
        }

        public static MonsterKoopa SetLEObject(LevelEditorObject le)
        {
            return new MonsterKoopa(le.x, le.y);
        }

        public override void Draw()
        {
            base.Draw();
        }
        public override void OnWalk(object sender, EventArgs e)
        {
            if (current == shield && IntersectsObjects.Count == 0)
                base.OnWalk(sender, e);
            current.OnWalk(sender, e);
            if (current == walking || current == shieldMoving)
                base.OnWalk(sender, e);
        }

        public override void OnAnimate(object sender, EventArgs e)
        {
            base.OnAnimate(sender, e);
            current.OnAnimate(sender, e);
        }

        public KoopaWalkingState GetWalkingState()
        {
            return walking;
        }

        public KoopaShieldState GetShieldState()
        {
            return shield;
        }

        public KoopaReturningState GetReturnState()
        {
            return returning;
        }

        public KoopaShieldMovingState GetShieldMovingState()
        {
            return shieldMoving;
        }

        public void KoopaSetState(IKoopaState state)
        {
            current = state;
            current.SetProperties();
        }

        public IKoopaState getCurrentState()
        {
            return current;
        }

        public MonsterKoopa(int x, int y)
            : base(OT: ObjectType.OT_Koopa, strategy: new KoopaStrategy())
        {
            this.x = x;
            this.y = y;
            ImageCount = 10;

            walking = new KoopaWalkingState(this);
            shield = new KoopaShieldState(this);
            returning = new KoopaReturningState(this);
            shieldMoving = new KoopaShieldMovingState(this);

            SetWidthHeight();

            current = walking;
            current.SetProperties();
            Dir = KoopaDir.KD_Right;

            TimerGenerator.AddTimerEventHandler(TimerType.TT_50, this.OnWalk);
            TimerGenerator.AddTimerEventHandler(TimerType.TT_100, this.OnAnimate);

        }
    }

}
