﻿using MarioObjects.Objects.Utils;
using System;
using static MarioObjects.Objects.GameObjects.MonsterKoopa;

namespace MarioObjects.Objects.GameObjects.State
{
    public class KoopaShieldMovingState : IKoopaState
    {
        MonsterKoopa monster;

        public KoopaShieldMovingState(MonsterKoopa monster)
        {
            this.monster = monster;
        }

        public void OnAnimate(object sender, EventArgs e)
        {

        }

        public void OnWalk(object sender, EventArgs e)
        {
            if (monster.newx <= LevelGenerator.CurrentLevel.MarioObject.x - 160)
            {
                if (monster.Live)
                {
                    monster.Animated = false;
                    monster.Live = false;
                    monster.Visible = false;
                }
            }

            

            if (monster.DirX > 0)
                monster.Dir = KoopaDir.KD_Right;
            else
                monster.Dir = KoopaDir.KD_Left;

        }

        public void SetProperties()
        {
            monster.width = 16;
            monster.height = 27;
            monster.WalkStep = 4;
            monster.AnimatedCount = 4;
            monster.OffsetIndex = 4;
            monster.Animated = true;
        }
    }
}
