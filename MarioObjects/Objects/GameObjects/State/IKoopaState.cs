﻿using System;

namespace MarioObjects.Objects.GameObjects.State
{
    public interface IKoopaState
    {
        void OnWalk(object sender, EventArgs e);

        void OnAnimate(object sender, EventArgs e);

        void SetProperties();
    }
}
