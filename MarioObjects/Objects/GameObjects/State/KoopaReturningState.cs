﻿using System;

namespace MarioObjects.Objects.GameObjects.State
{
    public class KoopaReturningState : IKoopaState
    {
        MonsterKoopa monster;

        public KoopaReturningState(MonsterKoopa monster)
        {
            this.monster = monster;
        }

        public void OnAnimate(object sender, EventArgs e)
        {
            monster.ReturningTime++;
            monster.ImageIndex = (monster.ReturningTime % 2) * 4 + 4; //4 or 9;

            if (monster.ReturningTime > 40)
            {
                monster.ReturningTime = 0;
                monster.KoopaSetState(monster.GetWalkingState());
            }
        }

        public void OnWalk(object sender, EventArgs e)
        {

        }

        public void SetProperties()
        {
            monster.OffsetIndex = 0;
        }
    }
}
