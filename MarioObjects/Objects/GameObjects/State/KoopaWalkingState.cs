﻿using MarioObjects.Objects.Utils;
using System;
using static MarioObjects.Objects.GameObjects.MonsterKoopa;

namespace MarioObjects.Objects.GameObjects.State
{
    public class KoopaWalkingState : IKoopaState
    {
        MonsterKoopa monster;

        public KoopaWalkingState(MonsterKoopa monster)
        {
            this.monster = monster;
        }

        public void OnAnimate(object sender, EventArgs e)
        {

        }

        public void OnWalk(object sender, EventArgs e)
        {
            if (monster.newx <= LevelGenerator.CurrentLevel.MarioObject.x - 160)
            {
                if (monster.Live)
                {
                    monster.Animated = false;
                    monster.Live = false;
                    monster.Visible = false;
                }
            }


            if (monster.DirX > 0)
                monster.Dir = KoopaDir.KD_Right;
            else
                monster.Dir = KoopaDir.KD_Left;

            switch (monster.Dir)
            {
                case KoopaDir.KD_Left:
                    {
                        monster.OffsetIndex = 0;
                    }
                    break;
                case KoopaDir.KD_Right:
                    {
                        monster.OffsetIndex = 2;
                    }
                    break;
            }
        }

        public void SetProperties()
        {
            monster.width = 16;
            monster.height = 27;
            monster.AnimatedCount = 2;
            monster.newy -= 11;
            monster.WalkStep = 1;
            monster.Animated = true;
        }
    }
}
