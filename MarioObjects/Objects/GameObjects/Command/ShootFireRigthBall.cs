﻿namespace MarioObjects.Objects.GameObjects.Command
{
    public class ShootFireRigthBall : IShootFireBall
    {
        private IFireBallGun gun;

        public ShootFireRigthBall(IFireBallGun gun)
        {
            this.gun = gun;
        }

        public void Execute(int x, int y)
        {
            gun.SetCordinates(x, y);
            gun.FireRigth();
        }
    }
}
