﻿namespace MarioObjects.Objects.GameObjects.Command
{
    class ShootFireLeftBall : IShootFireBall
    {
        private IFireBallGun gun;
        
        public ShootFireLeftBall(IFireBallGun gun)
        {
            this.gun = gun; 
        }

        public void Execute(int x, int y)
        {
            gun.SetCordinates(x, y);
            gun.FireLeft();
        }
    }
}
