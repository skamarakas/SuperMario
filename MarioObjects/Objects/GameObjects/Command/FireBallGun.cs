﻿using System.Collections.Generic;

namespace MarioObjects.Objects.GameObjects.Command
{
    class FireBallGun : IFireBallGun
    {
        private List<FireBall> FireBalls;

        public int FireBallIndex;

        private int x;
        private int y;

        public FireBallGun(List<FireBall> list)
        {
            FireBalls = list;
        }
        

        public void FireLeft()
        {
            if (!FireBalls[FireBallIndex].Started)
            {
                FireBalls[FireBallIndex].RunFireBall(x, y, FireBall.FireBallType.FT_Mario, FireBall.FireBallDir.FB_Left);
                FireBallIndex = (FireBallIndex + 1) % 2;
                Media.PlaySound(Media.SoundType.ST_FireBall);
            }
        }

        public void FireRigth()
        {
            if (!FireBalls[FireBallIndex].Started)
            {
                FireBalls[FireBallIndex].RunFireBall(x, y, FireBall.FireBallType.FT_Mario, FireBall.FireBallDir.FB_Right);
                FireBallIndex = (FireBallIndex + 1) % 2;
                Media.PlaySound(Media.SoundType.ST_FireBall);
            }
        }

        public void SetCordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
