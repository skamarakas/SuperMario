﻿namespace MarioObjects.Objects.GameObjects.Command
{
    public interface IFireBallGun
    { 
        void FireLeft();

        void FireRigth();

        void SetCordinates(int x, int y);
    }
}
