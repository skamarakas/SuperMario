﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarioObjects.Objects.GameObjects.DecoratorDP
{
   public abstract class MonsterDecorator:IMonster
    {
        private IMonster inner;
        public MonsterDecorator(IMonster some)
        {
            inner = some;
        }
        public virtual string GetName()
        {
           return inner.GetName();
        }
        public virtual int GetHeight()
        {
            return inner.GetHeight();
        }
        public virtual int GetWidth()
        {
           return inner.GetWidth();
        }
    }
}
