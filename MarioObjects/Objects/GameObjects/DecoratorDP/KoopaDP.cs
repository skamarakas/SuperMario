﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarioObjects.Objects.GameObjects.DecoratorDP
{
   public class KoopaDP : MonsterDecorator
    {
        public KoopaDP(IMonster some): base(some) {}
        public override string GetName()
        {
           return base.GetName() + "Koopa";
        }
        public override int GetHeight()
        {
                return base.GetHeight() + 16;
        }
        public override int GetWidth()
        {
                return base.GetWidth() + 27;
        }

    }
   
}
