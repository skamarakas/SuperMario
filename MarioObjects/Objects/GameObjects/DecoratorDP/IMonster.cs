﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarioObjects.Objects.GameObjects.DecoratorDP
{
    public interface IMonster
    {
        string GetName();
        int GetHeight();
        int GetWidth();
    }
}
