﻿using System;
using System.Drawing;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects.Strategy;
using MarioObjects.Objects.Patterns;

namespace MarioObjects.Objects.GameObjects.Adapter
{
    public class PiranahAdapter : IMoveableAnimatedObject
    {
        private MonsterPiranah monsta;

        public PiranahAdapter(MonsterPiranah monsta)
        {
            this.monsta = monsta;
        }

        public void AcceptVisitor(VisitorObject V)
        {
            monsta.AcceptVisitor(V);
        }

        public void AddCollision(Intersection IC)
        {
            monsta.AddCollision(IC);
        }

        public void AddObject(GraphicObject g)
        {
            monsta.AddObject(g);
        }

        public void BaseIntersection(Collision c, GraphicObject g)
        {
            //Not implemented
        }

        public void CheckObjectEnabled()
        {
            monsta.CheckObjectEnabled();
        }

        public void Draw()
        {
            monsta.Draw();
        }

        public Rectangle GetObjectRect()
        {
           return monsta.GetObjectRect();
        }

        public IIntersectionStrategy GetStrategy()
        {
            return monsta.GetStrategy();
        }

        public void Intersection(Collision c, GraphicObject g)
        {
            monsta.Intersection(c,g);
        }

        public void Intersection_None()
        {
            monsta.Intersection_None();
        }

        public void LogMe(Collision c, GraphicObject g)
        {
            monsta.LogMe(c, g);
        }

        public void OnAnimate(object sender, EventArgs e)
        {
            monsta.OnAnimate(sender, e);
        }

        public void OnWalk(object sender, EventArgs e)
        {
            monsta.OnMove(sender, e);
        }

        public void SetObjectChangeFlag(bool F)
        {
            monsta.SetObjectChangeFlag(F);
        }

        public void SetWidthHeight()
        {
            monsta.SetWidthHeight();
        }

        public MonsterPiranah GetPiranah()
        {
            return monsta;
        }
    }
}
