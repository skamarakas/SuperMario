﻿
using MarioObjects.Objects.BaseObjects;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    class GoombaStrategy : IIntersectionStrategy
    {
        public void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_Goomba:
                    {
                        ((MonsterGoomba)obj).DirX *= -1;
                        ((MonsterGoomba)obj).OnWalk(null, null);
                        ((MonsterGoomba)g).DirX *= -1;
                        ((MonsterGoomba)g).OnWalk(null, null);
                    }
                    break;
                case ObjectType.OT_Mario:
                    {
                        Mario m = (Mario)g;
                        if (c.Dir != CollisionDirection.CD_Down)
                        {
                            if (!m.Blinking)
                                if (m.Type == Mario.MarioType.MT_Big || m.Type == Mario.MarioType.MT_Fire)
                                {
                                    m.Type = Mario.MarioType.MT_Small;
                                    m.StartBlinking();
                                    m.SetMarioProperties();
                                }
                        }

                    }
                    break;
            }
        }
    }
}
