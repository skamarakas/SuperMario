﻿using MarioObjects.Objects.BaseObjects;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    class KoopaStrategy : IIntersectionStrategy
    {
        public void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_Brick:
                    {
                        if (((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetShieldMovingState())
                        {
                            if (c.Dir == CollisionDirection.CD_Left || c.Dir == CollisionDirection.CD_Right)
                                ((BlockBrick)g).BreakBrick();
                        }
                    }
                    break;
                case ObjectType.OT_Goomba:
                    {
                        if (((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetShieldMovingState())
                        {
                            ((MonsterGoomba)g).GoombaFallDie();
                        }
                        if (((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetWalkingState())
                        {
                            ((MonsterGoomba)g).DirX *= -1;
                            ((MonsterGoomba)g).newx += 5 * ((MonsterGoomba)g).DirX;

                            ((MonsterGoomba)g).OnWalk(null, null);
                            ((MonsterKoopa)obj).DirX *= -1;
                            ((MonsterKoopa)obj).OnWalk(null, null);
                        }
                        if (((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetShieldState())
                        {
                            ((MonsterGoomba)g).DirX *= -1;
                            ((MonsterGoomba)g).newx += 5 * ((MonsterGoomba)g).DirX;
                            ((MonsterGoomba)g).OnWalk(null, null);
                        }
                    }
                    break;
                case ObjectType.OT_Mario:
                    {

                        if (((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetShieldState() && ((MonsterKoopa)obj).ReturningTime >= 3)
                        {
                            if (c.Dir == CollisionDirection.CD_Left)
                                ((MonsterKoopa)obj).DirX = -1;

                            if (c.Dir == CollisionDirection.CD_Right)
                                ((MonsterKoopa)obj).DirX = 1;

                            ((MonsterKoopa)obj).KoopaSetState(((MonsterKoopa)obj).GetShieldMovingState());
                        }

                        // Size-down mario when colliding with a koopa
                        if (((MonsterKoopa)obj).getCurrentState() != ((MonsterKoopa)obj).GetShieldState()) // but not in shield state
                        {
                            if (!(((MonsterKoopa)obj).getCurrentState() == ((MonsterKoopa)obj).GetShieldMovingState() // Or that he's just set in motion
                                && (((MonsterKoopa)obj).DirX == -1 && c.Dir == CollisionDirection.CD_Left) || (((MonsterKoopa)obj).DirX == 1 && c.Dir == CollisionDirection.CD_Right)))
                            {
                                Mario m = (Mario)g;
                                if (c.Dir != CollisionDirection.CD_Down)
                                {
                                    if (!m.Blinking)
                                        if (m.Type == Mario.MarioType.MT_Big || m.Type == Mario.MarioType.MT_Fire)
                                        {
                                            m.Type = Mario.MarioType.MT_Small;
                                            m.StartBlinking();
                                            m.SetMarioProperties();
                                        }
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }
}
