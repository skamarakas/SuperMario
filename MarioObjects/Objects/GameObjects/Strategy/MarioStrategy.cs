﻿using MarioObjects.Objects.BaseObjects;
using static MarioObjects.Objects.GameObjects.Mario;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    class MarioStrategy : IIntersectionStrategy
    {
        public void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_Exit:
                    {
                        if (((Mario)obj).UpPressed)
                        {
                            ((Mario)obj).UpPressed = false;
                            System.Windows.Forms.MessageBox.Show("Very Good !");

                        }
                    }
                    break;
                case ObjectType.OT_Flower:
                    {
                        ((Flower)g).Visible = false;
                        if (((Mario)obj).Type != MarioType.MT_Fire)
                        {
                            ((Mario)obj).Type = MarioType.MT_Fire;
                            ((Mario)obj).SetMarioProperties();
                            Media.PlaySound(Media.SoundType.ST_Mush);
                        }

                    }
                    break;
                case ObjectType.OT_Mush:
                    {
                        ((MushRed)g).Visible = false;
                        ((MushRed)g).Animated = false;
                        ((MushRed)g).Live = false;
                        if (((Mario)obj).Type == MarioType.MT_Small)
                        {
                            ((Mario)obj).Type = MarioType.MT_Big;
                            ((Mario)obj).SetMarioProperties();
                            Media.PlaySound(Media.SoundType.ST_Mush);
                        }


                    }
                    break;

                case ObjectType.OT_Coin:
                    {
                        ((CoinBlock)g).Animated = false;
                        ((CoinBlock)g).Visible = false;
                        Media.PlaySound(Media.SoundType.ST_Coin);

                    }
                    break;
                case ObjectType.OT_Goomba:
                    {
                        if (c.Dir == CollisionDirection.CD_Up)
                        {
                            // Jump On Goomba with Control Presses
                            if (((MonsterGoomba)g).FallDie == false)
                            {
                                if (((Mario)obj).ControlPressed)
                                    ((Mario)obj).StartJump(true, 0);
                                else
                                    ((Mario)obj).StartJump(true, -20);

                                ((MonsterGoomba)g).GoombaDie();
                                Media.PlaySound(Media.SoundType.ST_Stomp);
                            }
                        }


                    }
                    break;
                case ObjectType.OT_Koopa:
                    {
                        if (c.Dir == CollisionDirection.CD_Up)
                        {
                            // Jump On Koopa with Control Presses
                            if (((MonsterKoopa)g).getCurrentState() == ((MonsterKoopa)g).GetWalkingState())
                            {
                                if (((Mario)obj).ControlPressed)
                                    ((Mario)obj).StartJump(true, 0);
                                else
                                    ((Mario)obj).StartJump(true, -20);

                                ((MonsterKoopa)g).KoopaSetState(((MonsterKoopa)g).GetShieldState());
                                Media.PlaySound(Media.SoundType.ST_Stomp);
                            }
                            else if ((((MonsterKoopa)g).getCurrentState() == ((MonsterKoopa)g).GetShieldState()) &&
                                    (((MonsterKoopa)g).ReturningTime >= 3))
                            {
                                ((MonsterKoopa)g).KoopaSetState(((MonsterKoopa)g).GetShieldMovingState());

                            }
                            else if (((MonsterKoopa)g).getCurrentState() == ((MonsterKoopa)g).GetShieldMovingState())
                            {
                                if (((Mario)obj).ControlPressed)
                                    ((Mario)obj).StartJump(true, 0);
                                else
                                    ((Mario)obj).StartJump(true, -20);

                                ((MonsterKoopa)g).KoopaSetState(((MonsterKoopa)g).GetShieldState());

                            }
                        }


                    }
                    break;


                case ObjectType.OT_MovingBlock:
                    goto case ObjectType.OT_Grass;

                case ObjectType.OT_SolidBlock:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_PipeUp:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_BlockQuestion:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_BlockQuestionHidden:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_Brick:
                    goto case ObjectType.OT_Grass;
                case ObjectType.OT_Grass:
                    {
                        ((Mario)obj).SetDirections();

                        if (c.Dir == CollisionDirection.CD_TopLeft)
                        {
                            if (g.OT == ObjectType.OT_Brick)
                            {
                                //if (MoveState == MarioMoveState.J_Right)
                                //    x -= (int)XAdd;
                                //if (MoveState == MarioMoveState.J_Left)
                                //    x += (int)XAdd;


                                //Intersection_None();
                            }


                        }
                        if (c.Dir == CollisionDirection.CD_Up)
                        {

                            if (g.OT == ObjectType.OT_MovingBlock)
                            {
                                ((Mario)obj).y = g.newy - ((Mario)obj).height;
                                ((BlockMoving)g).MarioOn = true;

                            }
                            else
                            {
                                if (((Mario)obj).State != MarioJumpState.J_None)
                                    ((Mario)obj).y = g.newy - ((Mario)obj).height;
                            }
                            if (((Mario)obj).State != MarioJumpState.J_None)
                                ((Mario)obj).State = MarioJumpState.J_None;
                            ((Mario)obj).SetDirections();

                        }

                        if (c.Dir == CollisionDirection.CD_Left)
                        {
                            ((Mario)obj).x = g.newx - ((Mario)obj).width;
                            //if (g.OT == ObjectType.OT_SolidBlock)
                            //    Intersection_None();
                            if (g.OT == ObjectType.OT_Brick)
                            {
                                //if (MoveState == MarioMoveState.J_Right)
                                //    x -= (int)XAdd;
                                //if (MoveState == MarioMoveState.J_Left)
                                //    x += (int)XAdd;
                                ((Mario)obj).x = g.newx - ((Mario)obj).width;


                            }

                        }

                        if (c.Dir == CollisionDirection.CD_Down)
                        {
                            if (((Mario)obj).State == MarioJumpState.J_Up)
                            {
                                ((Mario)obj).State = MarioJumpState.JDown;
                                ((Mario)obj).StartPosition = ((Mario)obj).y;
                                ((Mario)obj).TimeCount = 0;
                                ((Mario)obj).StartVelocity = 0;
                                if (g.OT == ObjectType.OT_BlockQuestion || g.OT == ObjectType.OT_BlockQuestionHidden)
                                {
                                    ((BlockQuestion)g).isMonsterExist();
                                    ((BlockQuestion)g).StartMove();
                                    if (((BlockQuestion)g).HiddenObject.OT != ObjectType.OT_Coin)
                                        Media.PlaySound(Media.SoundType.ST_Block);
                                }
                                if (g.OT == ObjectType.OT_Brick)
                                {
                                    if (((Mario)obj).Type == MarioType.MT_Big || ((Mario)obj).Type == MarioType.MT_Fire)
                                    {
                                        ((BlockBrick)g).BreakBrick();
                                        Media.PlaySound(Media.SoundType.ST_Brick);
                                    }
                                    else
                                    {
                                        Media.PlaySound(Media.SoundType.ST_Block);
                                    }

                                }
                            }

                        }
                        if (c.Dir == CollisionDirection.CD_Right)
                        {
                            ((Mario)obj).x = g.newx + g.width;
                            //if (g.OT == ObjectType.OT_SolidBlock)
                            //    Intersection_None();
                            //XAdd = 0;    
                        }

                    }
                    break;

            }
        }
    }
}
