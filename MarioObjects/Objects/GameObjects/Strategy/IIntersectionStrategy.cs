﻿using MarioObjects.Objects.BaseObjects;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    public interface IIntersectionStrategy
    {
        void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj);
    }
}
