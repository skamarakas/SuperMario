﻿using System;
using System.Collections.Generic;
using System.Text;
using MarioObjects.Objects.BaseObjects;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    public class NullObjectStrategy : IIntersectionStrategy
    {
        public void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj)
        {

        }
    }
}
