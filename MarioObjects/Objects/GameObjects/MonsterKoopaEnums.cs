﻿namespace MarioObjects.Objects.GameObjects
{
    public enum KoopaState { KS_Walking, KS_Shield, KS_Returning, KS_ShieldMoving };
    public enum KoopaDir { KD_Right, KD_Left };
}
