﻿using System.Collections.Generic;
using System.Data;

namespace MarioObjects.Objects.GameObjects.Memento
{
    public class MarioTypeCaretaker
    {
        List<MarioTypeMemento> savedTypes;

        public MarioTypeCaretaker(List<MarioTypeMemento> savedTypes)
        {
            this.savedTypes = savedTypes;
        }

        public void AddMemento(MarioTypeMemento memento)
        {
            savedTypes.Add(memento);
        }

        public MarioTypeMemento GetLastState()
        {
            if (savedTypes.Count != 0)
                return savedTypes[savedTypes.Count - 2];
            else
                return null;
        }
    }
}
