﻿using System;
using System.Collections.Generic;
using System.Text;
using Helper;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;

namespace MarioObjects.Objects.BaseObjects
{
    public abstract class AbstractBuildable
    {
        public List<GraphicObject> Objects;
        public Mario MarioObject;

        public void AddObject(GraphicObject g)
        {
            Objects.Add(g);
        }

    }
}
