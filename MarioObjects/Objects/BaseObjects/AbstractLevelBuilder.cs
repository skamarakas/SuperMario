﻿using System;
using System.Collections.Generic;
using System.Text;
using Helper;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;

namespace MarioObjects.Objects.BaseObjects
{
    public abstract class AbstractLevelBuilder: AbstractBuilder
    {
        private string LevelXmlSchema;

        public string getLevelXmlSchema()
        {
            return LevelXmlSchema;
        }

        public void setLevelXmlSchema(string schema)
        {
            LevelXmlSchema = schema;
        }

        public AbstractLevel getLevel()
        {
            return (AbstractLevel)this.getBuildable();
        }

        public new void build()
        {
            List<LevelEditorObject> list = MarioEditorXML.Load_From_XML(LevelXmlSchema);
            Mario MTemp = null;
            foreach (LevelEditorObject le in list)
            {
                Factory f = new Factory();
                GraphicObject g = f.SetEditorObject(le);
                if (g != null && g.OT != ObjectType.OT_Mario)
                    AddObject(g);
                else if (g.OT == ObjectType.OT_Mario)
                    MTemp = (Mario)g;

            }

            AddObject(MTemp);
            for (int i = 0; i < item.Objects.Count; i++)
                if (item.Objects[i].OT == ObjectType.OT_Mario)
                {
                    item.MarioObject = (Mario)item.Objects[i];
                    break;
                }
        }
    }
}
