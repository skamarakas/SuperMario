﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarioObjects.Objects.BaseObjects
{
    interface IBuilder
    {
        AbstractBuildable getBuildable();
        void build();
    }
}
