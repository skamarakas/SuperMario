﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;
using MarioObjects.Objects.Patterns;

namespace MarioObjects.Objects.BaseObjects
{
    interface ILevelBuilder
    {
        Level getLevel();
        void buildLevel();
    }
}
