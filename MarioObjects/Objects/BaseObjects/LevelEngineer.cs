﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarioObjects.Objects.BaseObjects
{
    class LevelEngineer
    {
        private AbstractLevelBuilder levelBuilder;

        public LevelEngineer(AbstractLevelBuilder levelBuilder)
        {
            this.levelBuilder = levelBuilder;
        }
        public void makeLevel()
        {
            levelBuilder.build();
        }
        public Level getLevel()
        {
            return (Level) levelBuilder.getBuildable();
        }
    }
}
