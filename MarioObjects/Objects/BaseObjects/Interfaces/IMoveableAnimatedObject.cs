﻿using System;

namespace MarioObjects.Objects.BaseObjects
{
    public interface IMoveableAnimatedObject : IAnimatedGraphicObject
    {
        void BaseIntersection(Collision c, GraphicObject g);
        void OnWalk(object sender, EventArgs e);
    }
}