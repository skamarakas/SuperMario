﻿using System;
using MarioObjects.Objects.GameObjects.Strategy;

namespace MarioObjects.Objects.BaseObjects
{
    public interface IAnimatedGraphicObject : IStaticGraphicObject 
    {
        IIntersectionStrategy GetStrategy();
        void OnAnimate(object sender, EventArgs e);
    }
}