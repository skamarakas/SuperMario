﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;
using MarioObjects.Objects.Patterns;

namespace MarioObjects.Objects.BaseObjects
{
    interface ILevel
    {
        Boolean Contains(Point Src, Rectangle Dest);
        Collision Intersects(GraphicObject SrcObject, GraphicObject DestObject);
        void AcceptVisitor(VisitorObject V);
        void UpdateY(int value);
        void Update_ScreensX();
        void Update_ScreensY();
        void DrawBackground();
        Rectangle GetAvailableObjectRec();
        void Draw();
    }
}
