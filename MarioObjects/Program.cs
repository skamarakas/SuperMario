using MarioObjects.Objects.GameObjects.DecoratorDP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace MarioObjects
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //creates basic monster
            IMonster monster = new BasicMonster();
            //basic into koopa
            MonsterDecorator decor = new KoopaDP(monster);
            //koopa in gooba
            decor = new GoombaDP(decor);
            Debug.Print(decor.GetName() + " " + decor.GetHeight() + " " + decor.GetWidth());
            Console.WriteLine(decor.GetName() + " " + decor.GetHeight() + " " + decor.GetWidth());

            //IMonster monster = new KoopaDP(new GoombaDP( new BasicMonster()));
            //Debug.Print(monster.getName + " " + monster.getHeight + " " + monster.getWidth);
            //Console.WriteLine(monster.getName + " " + monster.getHeight + " " + monster.getWidth);
            Application.Run(new frmMain());
        }
    }
}